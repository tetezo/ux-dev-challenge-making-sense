import { useEffect, useState } from 'react';
import Header from './components/Header';
import OverviewSection from './components/OverviewSection';
import SocialMediaSection from './components/SocialMediaSection';
import Modal from './components/Modal';
import styled from 'styled-components';



function App() { 
  const [theme, setTheme] = useState("dark"); //Constant of the color theme
  const [open, setOpen] = useState(false); //Constant of the modal state
  const [card, setCard] =useState({
    socialMedia: "",
    username: "",
    followers: "",
    typeFollows: "",
    today: "",
    status: "",
    last10Days: "",
    days: [{day: 4, followers: 5}]
  })//Constant of the social media card to sent to the modal

  useEffect(() => {
    if (theme === "dark") {
      document.documentElement.classList.add("dark");
    } else {
      document.documentElement.classList.remove("dark");
    }
  }, [theme]);//This event is executed every time the constant theme is changed

  const handleThemeSwitch = () => {
    setTheme(theme === "dark" ? "light" : "dark");
  }//This function changes the theme

  const openModal = (card: any) => {
    setCard(card);
    setOpen(true);
  }//This function opens the modal

  const Footer = styled.div`
    opacity: 1;
    pointer-events: all;
    &:before{
      content: '';
      display: block;
      width: 100%;
      height: 245px;
      border-radius: 0 0 20px 20px;
      position: absolute;
      top: 0;
      background: ${theme === 'dark' ? 'hsl(232, 19%, 15%);' : 'hsl(225, 100%, 98%);'}
      box-shadow: 0 -1px 2px 0 rgba(0,0,0,0.3);
      z-index: -1;
    }
  `//Styles for the BG top pattern


  return (
    <div className="relative font-inter py-8 bg-white-bg dark:bg-very-dark-blue-bg h-full min-h-screen z-10">
      <Header handleThemeSwitch={handleThemeSwitch}/>
      <SocialMediaSection onOpen={openModal}/>
      <OverviewSection />
      <Modal open={open} onClose={() => setOpen(false)} card={card}/>
      <Footer />
    </div> 
  );
}

export default App;
