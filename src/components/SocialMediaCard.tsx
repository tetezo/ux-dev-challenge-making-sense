import React from 'react'
import FacebookIcon from '../assets/icon-facebook.svg'
import InstagramIcon from '../assets/icon-instagram.svg';
import TwitterIcon from '../assets/icon-twitter.svg';
import YoutubeIcon from '../assets/icon-youtube.svg'
import ArrowUp from '../assets/icon-up.svg'
import ArrowDown from '../assets/icon-down.svg'

type socialMediaCard = {
    card: {
        socialMedia: string,
        username: string,
        followers: string,
        typeFollows: string,
        today: string,
        status: string,
        days: any[]
    },
    onOpen: (card: any) => void;
}//Types of props that the function social media card receives

const SocialMediaCard: React.FC<socialMediaCard> = ({card, onOpen}) => {
    const handleClick = (card: any) => {
        onOpen(card);
    }//Function that executes a function recieved from prop. This function opens the modal
  return (
    <div className={card.socialMedia === 'facebook' ? 'card-facebook' : card.socialMedia === 'instagram' ? 'card-instagram': card.socialMedia === 'twitter' ? 'card-twitter' : 'card-youtube' }>
        <div className="card-content" onClick={() => handleClick(card)}>
            <div className='flex flex-row items-center'>
                <img src={card.socialMedia === 'facebook' ? FacebookIcon : card.socialMedia === 'instagram' ? InstagramIcon: card.socialMedia === 'twitter' ? TwitterIcon : YoutubeIcon} className='mr-2' alt={card.socialMedia === 'facebook' ? 'facebook-icon' : card.socialMedia === 'instagram' ? 'instagram-icon': card.socialMedia === 'twitter' ? 'twitter-icon' : 'youtube-icon'} />
                <h5 className='text-sm font-bold text-dark-grayish-blue-t dark:text-desaturated-blue-t'>{card.username}</h5>
            </div>
            <div className='flex-col'>
                <h1 className='text-[56px] leading-none font-bold'>{card.followers}</h1>
                <h6 className='text-xs font-normal tracking-[6px] uppercase text-dark-grayish-blue-t dark:text-desaturated-blue-t'>{card.typeFollows}</h6>
            </div>
            <div className='flex flex-row items-center'>
                <img src={card.status === 'down' ? ArrowDown : ArrowUp} className='mr-1' alt='arrow-icon' />
                <h6 className={card.status === 'down' ? 'text-xs font-bold text-bright-red' : 'text-xs font-bold text-lime-green'}>{card.today}</h6>
            </div>
        </div>
    </div> 
  )
}

export default SocialMediaCard