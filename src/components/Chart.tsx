import React, { PureComponent } from 'react';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import CustomTooltipChart from './CustomTooltipChart';

type socialMediaCard = {
    card: {
        socialMedia: string,
        username: string,
        followers: string,
        typeFollows: string,
        today: string,
        days: {day: number, followers: number}[]
    }
  }//Type of object that the chart function can receive as a prop

const Chart: React.FC<socialMediaCard> = (card) =>{
    const CustomTooltipContent = ({ active, payload}: any) => {
        if (active && payload && payload.length) {
            const dataIndex = payload[0].value; 
            const text = dataIndex + ' new ' + card.card.typeFollows;
            return (
                <CustomTooltipChart text={text} />
            );
        }
    
        return null;
      };//Function to customize the tooltip
    return (
        <div style={{ width: '100%', height: 300, paddingLeft: '40px', paddingRight:'50px' }}>
            <ResponsiveContainer width="100%" height="100%">
                <LineChart
            data={card.card.days}>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="day" />
            <YAxis domain={[0, 12]} tickCount={7}/>
            <Tooltip content={<CustomTooltipContent />}/>
            <Line
                type="monotone"
                dataKey="followers"
                stroke="hsl(243, 51%, 70%)"
                activeDot={{ r: 8 }}
            />
            </LineChart>
    </ResponsiveContainer>
    </div>
    );
}

export default Chart;
