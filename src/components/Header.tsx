import React from 'react'
import Switch from './Switch'

interface HeaderFunction {
  handleThemeSwitch: () => void;
}//This interface defines the type of the prop that the function header receives

const Header: React.FC<HeaderFunction> = ({handleThemeSwitch}) => {
  return (
    <>
    <div className='sm:hidden mr-4 pb-2 flex flex-row justify-end items-center dark:hover:brightness-125'>
         <p className='mr-1.5 lg:mr-3.5 text-sm font-bold text-dark-grayish-blue-t dark:text-desaturated-blue-t'>Dark Mode</p>
        <Switch handleThemeSwitch={handleThemeSwitch}/>
    </div>
    <div className='flex flex-row justify-between items-center mx-10 
                    md:mx-20
                    lg:mx-32
                    xl:mx-44
                    xxl:mx-96'>
        <div className='flex flex-col items-start'>
            <h1 className='text-[28px] leading-9 font-bold text-very-dark-blue-t dark:text-white-t'>Social Media Dashboard</h1>
            <p className='text-sm font-bold text-dark-grayish-blue-t dark:text-desaturated-blue-t'>Total Followers: 23,004</p>
        </div>
        <div className='hidden sm:flex flex-row items-center dark:hover:brightness-125'>
          <p className='mr-1.5 lg:mr-3.5 text-sm font-bold text-dark-grayish-blue-t dark:text-desaturated-blue-t'>Dark Mode</p>
            <Switch handleThemeSwitch={handleThemeSwitch}/>
        </div>
    </div>
    </>
  )
}

export default Header