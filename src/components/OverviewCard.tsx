import React from 'react'
import FacebookIcon from '../assets/icon-facebook.svg'
import InstagramIcon from '../assets/icon-instagram.svg'
import TwitterIcon from '../assets/icon-twitter.svg'
import YoutubeIcon from '../assets/icon-youtube.svg'
import ArrowUp from '../assets/icon-up.svg'
import ArrowDown from '../assets/icon-down.svg'

type overviewCard = {
  card: {
    socialMedia: string,
        activity: string,
        number: string,
        status: string,
        percentage: string
  }
}//Type of prop that the function overview card receives

const OverviewCard: React.FC<overviewCard> = (card) => {
  return (
    <div className='overview-card'>
        <div className='flex flex-row justify-between items-center'>
            <h5 className='text-sm font-bold text-dark-grayish-blue-t dark:text-desaturated-blue-t'>{card.card.activity}</h5>
            <img src={card.card.socialMedia === 'facebook' ? FacebookIcon : card.card.socialMedia === 'instagram' ? InstagramIcon: card.card.socialMedia === 'twitter' ? TwitterIcon : YoutubeIcon} alt="facebook-icon" />
        </div>
        <div className='flex flex-row justify-between items-baseline'>
            <h2 className='text-[32px] font-bold text-very-dark-blue-t dark:text-white-t'>{card.card.number}</h2>
            <div className='flex flex-row items-center'>
                <img src={card.card.status === 'up' ? ArrowUp : ArrowDown} className='mr-1.5' alt="arrow-icon" />
                <h6 className={card.card.status === 'up' ? 'text-xs font-bold text-lime-green' : 'text-xs font-bold text-bright-red '}>{card.card.percentage}</h6>
            </div>
        </div>
    </div>
  )
}

export default OverviewCard