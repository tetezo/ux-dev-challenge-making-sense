import React from 'react'
import SocialMediaCard from './SocialMediaCard'
import data from '../data/socialMediaData.json'

type socialMediaCard = {
  socialMedia: string,
  username: string,
  followers: string,
  typeFollows: string,
  today: string,
  status: string,
  days: any[]
}//Type of prop that is sent to the social media card component

type modalFunction = {
  onOpen: (card: any) => void;
}//Type of prop that the function social media section receives

const SocialMediaSection: React.FC<modalFunction> = ({onOpen}) => {
  return (
    <div className='mt-12 mb-10 mx-6 flex flex-col items-center gap-y-8 flex-wrap justify-center
                    xs:justify-center
                    sm:mx-10 sm:flex sm:flex-row sm:justify-between 
                    md:mx-20 
                    lg:mx-32 lg:flex-row lg:justify-between
                    xl:mx-44
                    xxl:mx-96'>
        {
            data.map((card: socialMediaCard) => (
                <SocialMediaCard card={card} onOpen={onOpen}/>
            ))//Iteration of the social media card list. This list is obtained from the socialMediaData.json file
        }
    </div>
  )
}

export default SocialMediaSection