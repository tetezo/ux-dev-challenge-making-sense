import { Text } from 'recharts';

const CustomTooltipChart: React.FC<{text: string}> = ({ text}) => {

    return (
      <>
      <div className='border border-desaturated-blue-t dark:border-desaturated-purple-chart bg-white-bg dark:bg-very-dark-blue-bg'>
        <p className='text-sm font-normal text-desaturated-purple-chart py-2 px-2.5'>{text}</p>
      </div>
      </>
    );

};

export default CustomTooltipChart;