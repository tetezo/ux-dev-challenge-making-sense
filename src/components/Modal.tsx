import React, {useState} from 'react'
import FacebookIcon from '../assets/icon-facebook.svg'
import InstagramIcon from '../assets/icon-instagram.svg';
import TwitterIcon from '../assets/icon-twitter.svg';
import YoutubeIcon from '../assets/icon-youtube.svg'
import ArrowUp from '../assets/icon-up.svg'
import ArrowDown from '../assets/icon-down.svg'
import Chart from './Chart';


type propTypes = {
    open: boolean;
    onClose: () => void;
    card: {
        socialMedia: string,
        username: string,
        followers: string,
        typeFollows: string,
        today: string,
        status: string,
        last10Days: string,
        days: {day: number, followers: number}[]
    },
}//Types of props that the function modal receives

const Modal: React.FC<propTypes> = ({open, onClose, card}) => {
    

  return (
    <div className={`fixed inset-0 flex justify-center items-center transition-colors ${open ? "visible bg-black/50" : "invisible"}`} onClick={onClose}>
        <div className={`flex flex-col h-[75%] xl:h-auto w-[68%] rounded-3xl bg-very-pale-blue-top-bg dark:bg-very-dark-blue-top-bg text-very-dark-blue-t dark:text-white-t shadow transition-all overflow-y-scroll xl:overflow-y-hidden`} onClick={(e) => e.stopPropagation()}>
            <button className='flex justify-end pt-9 pr-9' onClick={onClose}>
                <svg className='stroke-very-dark-blue-t dark:stroke-white-t' xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                    <path d="M6 18L18 6M6 6L18 18" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
                </button>
            <div className='flex flex-col justify-between bg-very-pale-blue-top-bg dark:bg-very-dark-blue-top-bg rounded-3xl pl-10 xl:pl-20 pb-4'>
                <div className='flex flex-row justify-between items-center pb-6'>
                    <h2 className='text-[30px] md:text-4xl font-bold text-very-dark-blue-t dark:text-white-t'>{card.socialMedia.charAt(0).toUpperCase()+ card.socialMedia.slice(1)} {card.typeFollows}</h2>
                </div>            
                <div className='flex flex-row items-center'>
                    <img src={card.socialMedia === 'facebook' ? FacebookIcon : card.socialMedia === 'instagram' ? InstagramIcon: card.socialMedia === 'twitter' ? TwitterIcon : YoutubeIcon} className='mr-3.5' alt={card.socialMedia === 'facebook' ? 'facebook-icon' : card.socialMedia === 'instagram' ? 'instagram-icon': card.socialMedia === 'twitter' ? 'twitter-icon' : 'youtube-icon'} />
                    <h4 className='text-lg font-bold text-desaturated-blue-t'>{card.username}</h4>
                </div>
                <div className='flex flex-col items-start gap-x-14 pr-8
                                xl:flex-row xl:items-center'>
                    <div className='xl:w-72 w-auto flex flex-col md:flex-row md:items-center gap-x-6'>
                        <h1 className='text-[52px] font-bold text-dark-grayish-blue-t dark:text-white-t'>{card.followers}</h1>
                        <h4 className='text-lg leading-5 font-normal text-dark-grayish-blue-t dark:text-white-t'>Total {card.typeFollows}</h4>
                    </div>
                    <div className='xl:w-72 w-auto flex flex-col md:flex-row md:items-center gap-x-6'>
                        <div className='flex flex-row items-center'>
                            <img src={ArrowUp} className='w-[12px] h-[8px] mr-2' alt="arrowUp-icon" />
                            <h1 className='text-[52px] font-bold text-lime-green'>{card.last10Days}</h1>
                        </div>
                        <h4 className='text-lg leading-5 font-normal text-dark-grayish-blue-t dark:text-white-t'>New followers in the past 10 days</h4>
                    </div>
                    <div className='xl:w-72 w-auto flex flex-col md:flex-row md:items-center gap-x-6'>
                        <div className='flex flex-row items-center'>
                            <img src={card.status === 'down' ? ArrowDown : ArrowUp} className='w-[12px] h-[8px] mr-2' alt="arrowUp-icon" />
                            <h1 className={card.status === 'down' ? 'text-[52px] font-bold text-bright-red' : 'text-[52px] font-bold text-lime-green'}>{card.today.split(" ")[0]}</h1>
                        </div>
                        <h4 className='text-lg leading-5 font-normal text-dark-grayish-blue-t dark:text-white-t'>New followers TODAY</h4>
                    </div>
                </div>
            </div>
            <div className='flex flex-col bg-white-bg dark:bg-very-dark-blue-bg rounded-3xl pb-8'>
                <h3 className='text-2xl font-normal tracking-tight text-dark-grayish-blue-t dark:text-desaturated-blue-t py-10 pl-10 xl:pl-20'>May 4 - May 13</h3>
                <Chart card={card}/>
            </div>

            
            

        </div>
    </div>
  )
}

export default Modal