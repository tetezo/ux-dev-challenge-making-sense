import React, { useState } from 'react'
import styled from 'styled-components';

type switchFunction = {
  handleThemeSwitch: () => void;
}//Type of prop that the function switch receives

//Constants to give style to the switch toggle
const Label = styled.label`
  position: relative;
  width: 48px;
  height: 24px;
`;

const SpanClass = styled.span`
  position: absolute;
  cursor: pointer;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: end;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background-color: hsl(230, 22%, 74%);
  transition: 0.4s;
  border-radius: 12px;
    &:before {
      position: absolute;
      content: "";
      height: 18px;
      width: 18px;
      left: 4px;
      background-color: white;
      transition: 0.4s;
      border-radius: 12px;
      transform: translateX(22px);
    }
    &:hover{
      background: linear-gradient(to right, hsl(210, 78%, 56%), hsl(146, 68%, 55%));
    }
`

const InputSwitch = styled.input`
  opacity: 0;
  width: 0;
  height: 0;
  &:checked + ${SpanClass} {
    background: linear-gradient(to right, hsl(210, 78%, 56%), hsl(146, 68%, 55%));
    &:before {
      transform: translateX(0px);
      background-color: hsl(228, 28%, 20%);
    }
  }
`

const Switch: React.FC<switchFunction> = ( {handleThemeSwitch}) => {
  return (
    <Label>
        <InputSwitch type="checkbox" 
              defaultChecked={true} 
              onClick={handleThemeSwitch} />
        <SpanClass />
    </Label>
  )
}

export default Switch