import React from 'react'
import OverviewCard from './OverviewCard'
import cards from '../data/overviewData.json'

type overviewCard = {
  socialMedia: string,
  activity: string,
  number: string,
  status: string,
  percentage: string
  
}//Type of prop that is sent to the overview card component

const OverviewSection = () => {
  return (
    <div className='mx-6
                    sm:mx-10 
                    md:mx-20
                    lg:mx-32
                    xl:mx-44
                    xxl:mx-96'>
        <h2 className='text-[24px] text-very-dark-blue-t dark:text-white-t font-bold'>OverView - Today</h2>
        <div className='mt-6 flex flex-row justify-center flex-wrap gap-y-6
                        xs:flex xs:flex-col xs:items-center
                        sm:flex sm:flex-row sm:justify-between
                        xl:flex xl:flex-row xl:justify-between
                        xxl:justify-between '>
            {
                cards.map((card: overviewCard) => (
                    <OverviewCard  card={card}/>
                ))//Iteration of the overview card list. This list is obtained from the overviewData.json file
            }
        </div>
    </div>
  )
}

export default OverviewSection