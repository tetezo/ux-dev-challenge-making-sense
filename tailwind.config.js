/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js,jsx,ts,tsx}"],
  darkMode: "class",
  theme: {
    extend: {
      fontFamily:{
        "inter": ["Inter", 'sans-serif'],
      },
      colors: {
        //primary
        "lime-green": "hsl(163, 72%, 41%)",
        "bright-red": "hsl(356, 69%, 56%)",
        "facebook": "hsl(195, 100%, 50%)",
        "twitter": "hsl(203, 89%, 53%)",
        "youtube": "hsl(348, 97%, 39%)",
        //dark theme
        "very-dark-blue-bg": "hsl(230, 17%, 14%)",
        "very-dark-blue-top-bg": "hsl(232, 19%, 15%)",
        "dark-desaturated-blue-card-bg": "hsl(228, 28%, 20%)",
        "desaturated-blue-t": "hsl(228, 34%, 66%)",
        "white-t": "hsl(0, 0%, 100%)",
        "desaturated-purple-chart": "hsl(243, 51%, 70%)",
        //light theme
        "toggle-light": "hsl(230, 22%, 74%)",
        "white-bg": "hsl(0, 0%, 100%)",
        "very-pale-blue-top-bg": "hsl(225, 100%, 98%)",
        "light-grayish-blue-card-bg": "hsl(227, 47%, 96%)",
        "dark-grayish-blue-t": "hsl(228, 12%, 44%)",
        "very-dark-blue-t": "hsl(230, 17%, 14%)",
        "desaturated-purple-chart": "hsl(243, 51%, 70%)",
      },
      backgroundImage: {
        "instagram": "linear-gradient(to right, hsl(37,97%, 70%), hsl(329, 70%, 58%))",
        "toggle-dark": "linear-gradient(to right, hsl(210, 78%, 56%), hsl(146, 68%, 55%))",
      }
    },
    screens: {
      xs: "480px",
      sm: "640px",
      md: "768px",
      lg: "1024px",
      xl: "1440px",
      xxl: "1536px",
    }
  },
  plugins: [],
}

