## Available Scripts

In the project directory, you can run:
### `npm install` 
This installs all the dependencies I used
### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

###

First I defined the different components of the page.
I decided to make a Header that contains the switch, then a section for the Social Media Dashboard and another section for Overview - Today.
I made isolated components so I could customize them better. For example the social media cards, the overview cards, the switch, the chart tooltip.
I used the Tailwind CSS framework, the SimpleLineChart library and the Styled Components library.
I used json to shape the data.
To structure the layout and the components I used flexbox because in my opinion it's easier to orient the elements.
I used Tailwind CSS classes to format and style components.
I used Styled Components (for example in the Switch) to style and use CSS pseudo elements.
To manage the responsive design I defined different breakpoints to adapt margin and padding.